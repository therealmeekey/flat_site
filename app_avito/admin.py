from django.contrib import admin
from app_avito.models import *


class NumbersInline(admin.TabularInline):
    model = Numbers
    insert_after = 'name'
    extra = 1


class LinkAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', 'url')


class AddsAdmin(admin.ModelAdmin):
    list_display = ('owner', 'title', 'price', 'contact', 'phone', 'date')
    ordering = ['-date']


class AgencyAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [NumbersInline]


admin.site.register(Agency, AgencyAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(Adds, AddsAdmin)

admin.site.site_header = 'Сайт Администратора'
admin.site.site_title = 'Сайт Администратора'
admin.site.index_title = 'Администратор сайта'
