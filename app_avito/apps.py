from django.apps import AppConfig


class AppAvitoConfig(AppConfig):
    name = 'app_avito'
    verbose_name = 'Поиск из Avito'
