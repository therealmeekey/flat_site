from django.db import models
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User
import re

# python manage.py crontab add
# python manage.py crontab show
# python manage.py crontab remove

SEARCH_TYPE = (
    ('0', 'Все'),
    ('1', 'Новые')
)

TIME_TYPE = (
    ('0', '*/5 * * * *'),
    ('1', '*/10 * * * *'),
    ('2', '*/20 * * * *')
)


class Link(models.Model):
    class Meta:
        verbose_name = 'Ссылка'
        verbose_name_plural = 'Ссылки'

    name = models.CharField(max_length=255, null=True, verbose_name='Название')
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True, verbose_name='Автор')
    url = models.URLField(verbose_name='URL')
    typex = models.CharField(choices=SEARCH_TYPE, null=True, max_length=16, verbose_name='Тип')
    time = models.CharField(choices=TIME_TYPE, null=True, max_length=16, verbose_name='Цикличность')

    def __str__(self):
        name = '{}. Автор: {}'.format(self.name, self.author.username)
        return name


class Agency(models.Model):
    class Meta:
        verbose_name = 'Нежелательный номер'
        verbose_name_plural = 'Нежелательные номера'

    name = models.CharField(max_length=255, null=True, blank=True, verbose_name='Название')

    def __str__(self):
        return self.name


class Numbers(models.Model):
    class Meta:
        verbose_name = 'Номер телефона'
        verbose_name_plural = 'Номера телефонов'

    agency = models.ForeignKey('Agency', on_delete=models.CASCADE)
    number = models.CharField(max_length=16, null=True, blank=True, verbose_name='Номер телефона')

    def __str__(self):
        return 'Номер телефона: ' + self.number

    def save(self, *args, **kwargs):
        self.number = re.sub(r'[^\d]+', '', str(self.number))
        adds = Adds.objects.filter(phone=self.number)
        for add in adds:
            add.delete()
        super(Numbers, self).save(*args, **kwargs)


class Adds(models.Model):
    class Meta:
        verbose_name = 'Объявление'
        verbose_name_plural = 'Объявления'

    owner = models.ForeignKey(Link, on_delete=models.CASCADE, null=True, verbose_name='Владелец')
    avito_id = models.IntegerField(null=True, blank=True, verbose_name='ID')
    title = models.CharField(max_length=255, null=True, blank=True, verbose_name='Название')
    price = models.PositiveIntegerField(validators=[MinValueValidator(0)], null=True, blank=True, verbose_name='Цена')
    url = models.URLField(max_length=255, null=True, blank=True, verbose_name='URL')
    address = models.CharField(max_length=255, null=True, blank=True, verbose_name='Адрес')
    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    date = models.CharField(max_length=255, null=True, blank=True, verbose_name='Дата публикации')
    contact = models.CharField(max_length=128, null=True, blank=True, verbose_name='Контактное лицо')
    phone = models.CharField(max_length=16, null=True, blank=True, verbose_name='Телефон')
    image = models.URLField(max_length=255, null=True, blank=True, verbose_name='Изображение')

    def __str__(self):
        name = '{}. Владелец: {}'.format(self.title, self.owner.author.username)
        return name
