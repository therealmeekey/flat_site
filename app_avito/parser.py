import requests
from bs4 import BeautifulSoup
from app_avito.models import *
from fake_useragent import UserAgent
import re


def check_ads(ads):
    try:
        numbers = Numbers.objects.get(number=ads['phone'])
        print(numbers)
    except Exception:
        link = Link.objects.get(author_id=ads['owner'])
        add, created = Adds.objects.get_or_create(id=int(ads['id']), owner=link)
        if created:
            add.owner = link
            add.id = int(ads['id'])
            add.title = ads['title']
            add.price = int(ads['price'])
            add.date = ads['date']
            add.url = ads['url']
            add.description = ads['description']
            add.address = ads['address']
            add.contact = ads['contact']
            add.phone = ads['phone']
            add.image = ads['image']
            add.save()
            print(ads['id'], add)


def get_html(url):
    user_a = UserAgent()
    headers = {
        'User-Agent': user_a.random
    }
    response = requests.get(url, headers=headers)
    return response.content


def get_ads_list():
    links = Link.objects.filter(typex='1')
    for link in links:

        url_list = link.url.split('.')
        if url_list[0] != 'https://m':
            html = get_html(link.url.replace('https://www', 'https://m'))
        else:
            html = get_html(link.url)

        soup = BeautifulSoup(html, 'lxml')
        div = soup.find('div', {'data-marker': 'items/list'})
        ads = div.find_all('div', {'class': '_328WR'})

        for ad in ads:

            id_s = ad.get('data-marker').split('(')
            id = id_s[1].replace(')', '')
            title = ad.find('span', {'data-marker': 'item/title'}).text.strip()
            price_s = ad.find('div', {'data-marker': 'item/price'}).text.strip().split(' ')
            image = ad.find('img', {'data-marker': 'item/image'})
            price = int((price_s[0] + price_s[1]).replace(" ", ''))
            date = ad.find('div', {'data-marker': 'item/datetime'}).text.strip()

            url_to_item = 'https://m.avito.ru' + ad.find('a', {'data-marker': 'item/link'}).get('href')
            html_ad = get_html(url_to_item)
            soup = BeautifulSoup(html_ad, 'lxml')

            url = url_to_item.replace('m.', 'www.')
            address = soup.find('span', {'data-marker': 'delivery/location'}).text.strip()
            contact = soup.find('span', {'data-marker': 'item-contact-bar/name'}).text.strip()
            description = soup.find('div', {'data-marker': 'item-description/text'}).text.strip()

            try:
                phone_s = soup.find('a', {'data-marker': 'item-contact-bar/call'}).get('href')
                phone = re.sub(r'[^\d]+', '', str(phone_s))
            except Exception:
                phone = ''

            ads = {
                'owner': link.author_id,
                'id': id,
                'title': title,
                'price': price,
                'date': date,
                'image': image,
                'description': description,
                'url': url,
                'address': address,
                'contact': contact,
                'phone': phone
            }
            check_ads(ads)
