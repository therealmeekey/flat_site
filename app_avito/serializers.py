from rest_framework import serializers
from app_avito.models import *
from django.contrib.auth.models import User


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class LinkSerializers(serializers.ModelSerializer):
    author = UserSerializers()

    class Meta:
        model = Link
        fields = ['name', 'author', 'url', 'typex']


class AddsSerializer(serializers.ModelSerializer):
    owner = LinkSerializers()

    class Meta:
        model = Adds
        fields = ['owner', 'title', 'price', 'url', 'address', 'date', 'contact', 'phone']
