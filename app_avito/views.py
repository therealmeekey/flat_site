from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from app_avito.serializers import *
from app_avito.models import *


def avito_list(request):
    avitos = Link.objects.all()
    return render(request, 'avito.html', {'avito': avitos})


class AvitoListView(APIView):

    def get(self, request, format=None):
        queryset = Link.objects.filter(author=request.user)
        serializer = LinkSerializers(queryset, many=True)
        return Response(serializer.data)


class AddsListView(APIView):

    def get(self, request, format=None):

        queryset = Adds.objects.filter(owner__author=request.user)
        serializer = AddsSerializer(queryset, many=True)
        return Response(serializer.data)
