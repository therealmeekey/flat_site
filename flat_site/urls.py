from django.contrib import admin
from django.urls import path
from app_avito.views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from flat_site import settings
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', AvitoListView.as_view()),
    path('adds/', AddsListView.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)

